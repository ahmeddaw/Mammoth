import ninja.sakib.pultusorm.annotations.AutoIncrement
import ninja.sakib.pultusorm.annotations.PrimaryKey

class Category() {
    @PrimaryKey
    @AutoIncrement
    var categoryId: Int = 0
    var name: String = "Default Name"
    var maxDownloads: Int = 3
    var outputDir: String = System.getProperty("user.home") + "/Downloads"
}