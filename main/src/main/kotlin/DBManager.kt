import ninja.sakib.pultusorm.core.PultusORM
import ninja.sakib.pultusorm.core.PultusORMUpdater

class DBManager {

    private val database: PultusORM = PultusORM("mammoth.db", "/Users/useruser/IdeaProjects/files")

    init {

//        val student: Student = Student()
//        student.name = "Sakib Sayem"
//        student.department = "CSE"
//        student.cgpa = 2.3
//        student.dateOfBirth = Date()
//        database.save(student)
//        database.close()
    }


    fun getCategorys(): ArrayList<String>{
        val list = ArrayList<String>()
        val categorys = database.find(Category())

        categorys.forEach {
            val cat = it as Category
            list.add(cat.name)
        }


        return list
    }


    fun createCategory(name: String, outputDir: String?=null, maxDownloads :Int = 3): Category? {
        val category = Category()
        category.name = name
        if (!outputDir.isNullOrBlank())
            category.outputDir= outputDir!!

        category.maxDownloads = maxDownloads
        return if (database.save(category))
            category
        else
            null
    }


    fun createItem(title: String, url: String ,category: Category?= null): DownloadItem? {
        val item = DownloadItem()
        item.title = title
        item.url = url
        if(category != null)
            item.categoryId = category.categoryId
        return if(database.save(item))
            item
        else
            null
    }

    fun getItems(category: Category? = null): List<DownloadItem>{
        val list = ArrayList<DownloadItem>()
        val da = database.find(DownloadItem())
        da.forEach {
            list.add(it as DownloadItem)
        }


        return list
    }


    fun refresh(item: DownloadItem): Unit {
        val updater = PultusORMUpdater.Builder()
                .set("progress",0)
                .build()
        database.update(item, updater)
    }
}