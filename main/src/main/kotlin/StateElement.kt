data class StateElement (
    var item:DownloadItem,
    var count : Float,
    var total : Float,
    var state : DownloadStates

)