import org.apache.http.NameValuePair
import org.apache.http.client.methods.HttpGet
import org.apache.http.client.protocol.HttpClientContext
import org.apache.http.client.utils.URIBuilder
import org.apache.http.client.utils.URLEncodedUtils
import org.apache.http.impl.client.BasicCookieStore
import org.apache.http.impl.client.HttpClientBuilder
import org.apache.http.impl.client.HttpClients
import org.apache.http.message.BasicNameValuePair
import org.apache.http.protocol.BasicHttpContext
import java.io.*
import java.net.URI
import java.net.URISyntaxException
import java.util.*
import java.util.regex.Pattern
import kotlin.collections.ArrayList

class DownloadTask(private val url: String, private val outdir: String = "/home/ahmed/IdeaProjects/Mammoth/files") : Thread() {
    //    private var item : DownloadItem = DownloadItem()
    private val pause = false
    val list = Vector<OnProgressListener>()
    private val progressListeners = ArrayList<DownloadTask.OnProgressListener>()
    private val postListeners = ArrayList<DownloadTask.OnPostListener>()
    private val afterListeners = ArrayList<DownloadTask.OnAfterListener>()
    private val scheme = "http"
    private val host = "www.youtube.com"
    private val commaPattern = Pattern.compile(",")
    private val userAgent = "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.13) Gecko/20101203 Firefox/3.6.13"
    public var filename: String = ""
//    private val outdir = "/home/ahmed/IdeaProjects/Mammoth/files"

    // '\f',
//    private val illegalFileNameCharacters = charArrayOf('/', '\n', '\r', '\t', '\u0000', '`', '?', '*', '\\', '<', '>', '|', '\"', ':')
    private val illegalFileNameCharacters = charArrayOf('/', '\n', '\r', '\t', '\u0000', '`', '?', '*', '\\', '<', '>', '|', '\"', ':')

    constructor(item: DownloadItem) : this(item.url!!, item.downloadDir)


    override fun run() {
        for (i in postListeners)
            i.onPostListener("STARTED")
        if (url.toLowerCase().contains("youtube.com/watch?v=")) {
            println("Youtube Start")
            startYouTubeVideo()
        } else {
            println("File Start")
            startFileDownloading()
        }


        for (i in afterListeners)
            i.onAfterListener("Finished")
//        super.start()
    }


    private fun startYouTubeVideo() {

//            usage("Missing video id. Extract from http://www.youtube.com/watch?v=VIDEO_ID")

        try {
            // default temp Dir
            // System.getProprty("java.io.tmpdir")

            val videoId: String = url.substring(url.indexOf("?v=") + 3)
            val format = Resolutions.R_HD720_MP4 // http://en.wikipedia.org/wiki/YouTube#Quality_and_codecs
            val encoding = "UTF-8"
            val outputDir = File(outdir)
            val extension = format.extention

            play(videoId, format.code, encoding, userAgent, outputDir, extension)

        } catch (t: Throwable) {
            t.printStackTrace()
        }

    }

    private fun startFileDownloading() {
        val filename = url.substring(url.lastIndexOf("/") + 1)
        val output = File("$outdir/$filename")
        downloadWithHttpClient(userAgent, url, output)
    }

    @Throws(Throwable::class)
    private fun play(videoId: String, format: Int, encoding: String, userAgent: String, outputdir: File, extension: String, quality: QualityCodes = QualityCodes.HD720) {
        val qparams = ArrayList<NameValuePair>()
        qparams.add(BasicNameValuePair("video_id", videoId))
        qparams.add(BasicNameValuePair("fmt", "" + format))
        val uri = getUri("get_video_info", qparams)

        val cookieStore = BasicCookieStore()
        val localContext = BasicHttpContext()
        localContext.setAttribute(HttpClientContext.COOKIE_STORE, cookieStore)

        val httpclient = HttpClientBuilder.create().build()
        val httpget = HttpGet(uri)
        httpget.setHeader("User-Agent", userAgent)

        val response = httpclient.execute(httpget, localContext)
        val entity = response.entity
        if (entity != null && response.statusLine.statusCode == 200) {
            val instream = entity.content
            val videoInfo = getStringFromInputStream(encoding, instream)
            if (videoInfo != null && videoInfo.isNotEmpty()) {
                val infoMap = URLEncodedUtils.parse(videoInfo, Charsets.UTF_8) as ArrayList<NameValuePair>
//                var token: String? = null
                var downloadUrl: String? = null
                var filename = videoId

                for (pair in infoMap) {
                    val key = pair.name
                    val `val` = pair.value
                    when (key) {
//                        "token" -> token = `val`
                        "title" -> filename = `val`
                        "url_encoded_fmt_stream_map" -> {
                            val formats = commaPattern.split(`val`)
                            for (fmt in formats) {
                                val fmts = listToHashMap(URLEncodedUtils.parse(fmt, Charsets.UTF_8))
                                if (fmts["quality"] == quality.quality) {
                                    downloadUrl = fmts["url"]

//                                    val quality = fmts["quality"]
                                    //codec 4 : "type=video/mp4; codecs="avc1.64001F, mp4a.40.2""
                                }

                            }
                        }
                    }
                }

                filename = cleanFilename(filename)
                if (filename.isEmpty()) {
                    filename = videoId
                } else {
                    filename += "_$videoId"
                }
                filename += ".$extension"
                this.filename = filename
                val outputfile = File(outputdir, filename)

                if (downloadUrl != null) {
                    downloadWithHttpClient(userAgent, downloadUrl, outputfile)
                }
            }
        }
    }

    @Throws(Throwable::class)
    private fun downloadWithHttpClient(userAgent: String, downloadUrl: String?, outputfile: File) {
        val httpget2 = HttpGet(downloadUrl)
        httpget2.setHeader("User-Agent", userAgent)

        val httpclient = HttpClients.createDefault()
        val response2 = httpclient.execute(httpget2)
        val entity2 = response2.entity
        if (entity2 != null && response2.statusLine.statusCode == 200) {
            val length = entity2.contentLength
            val instream2 = entity2.content
            if (outputfile.exists()) {
                outputfile.delete()
            }
            val outstream = FileOutputStream(outputfile)
            var downloaded = 0
            outstream.use { stream ->
                val buffer = ByteArray(2048)
                var count = instream2.read(buffer)
                while ((count) != -1) {
                    stream.write(buffer, 0, count)
                    downloaded += count
                    print("Download : ${downloaded.toFloat() / length.toFloat()}%")
                    println(" --> $downloaded/$length")

                    for (listner in progressListeners)
                        listner.onProgressListener(downloaded.toFloat(), length.toFloat())



                    count = instream2.read(buffer)
                }
                stream.flush()
            }
        }
    }

    private fun cleanFilename(filename: String): String {
        var cleanedName = filename
        for (c in illegalFileNameCharacters) {
            cleanedName = cleanedName.replace(c, '_')
        }
        return cleanedName
    }

    @Throws(URISyntaxException::class)
    private fun getUri(path: String, qparams: List<NameValuePair>): URI {
//        return URIUtils.createURI(scheme, host, -1, "/$path", URLEncodedUtils.format(qparams, "UTF-8"), null)
        return URIBuilder().apply {
            host = this@DownloadTask.host
            port = -1
            setPath("/$path")
            scheme = this@DownloadTask.scheme
            setCustomQuery(URLEncodedUtils.format(qparams, "UTF-8"))
        }.build()
    }


    @Throws(UnsupportedEncodingException::class, IOException::class)
    private fun getStringFromInputStream(encoding: String, instream: InputStream): String? {
        val writer = StringWriter()

        val buffer = CharArray(1024)
        instream.use { stream ->
            val reader = BufferedReader(InputStreamReader(stream, encoding))
            var n: Int
            n = reader.read(buffer)
            while (n != -1) {
                writer.write(buffer, 0, n)
                n = reader.read(buffer)
            }
        }
        return writer.toString()
    }


    private fun listToHashMap(list: MutableList<NameValuePair>): HashMap<String, String> {
        val map = HashMap<String, String>()
        list.forEach {
            map[it.name] = it.value
        }
        return map
    }


    public fun addOnProgressListener(listner: OnProgressListener): Unit {
        progressListeners.add(listner)
    }

    fun addOnProgressListener(onProgressListener: (count: Float, total: Float) -> Unit) {

        progressListeners.add(object : OnProgressListener {
            override fun onProgressListener(count: Float, total: Float) {
                onProgressListener(count, total)
            }

        })
    }

    fun addOnPostListener(onPostListener: (query: String) -> Unit) {

        postListeners.add(object : OnPostListener {
            override fun onPostListener(query: String) {
                onPostListener(query)
            }
        })
    }

    fun addOnAfterListener(onAfterListner: (query: String) -> Unit) {
        afterListeners.add(object : OnAfterListener {
            override fun onAfterListener(query: String) {
//                this.onAfterListener(query)
                onAfterListner(query)
            }


        })
    }


    interface OnProgressListener {
        fun onProgressListener(count: Float, total: Float)
    }

    interface OnPostListener {
        fun onPostListener(query: String)
    }

    interface OnAfterListener {
        fun onAfterListener(query: String)
    }
}

