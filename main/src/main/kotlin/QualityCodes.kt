enum class QualityCodes(val quality : String) {
    HD720("hd720"),
    MEDIUM("medium"),
    LOW("small"),
}