import javafx.application.Platform
import java.io.BufferedInputStream
import java.net.InetAddress
import java.net.ServerSocket
import kotlin.concurrent.thread

class MammothService(val main: Main) : Thread(){

    var taskManager: DownLoadTaskManager = main.taskManager
//    lateinit var ui : Main

//    companion object {
//        @JvmStatic
//        fun main(args: Array<String>) {
//            MammothService(this).start()
//        }
//    }

    override fun run() {
//        thread {
//            println("Mammoth service started")
//            taskManager = DownLoadTaskManager()
//            ui = Main()
//            ui.taskManager = taskManager
//            ui.launch()
//        }

//        val data = taskManager

        try {
            val port = 4848
            val address = InetAddress.getLocalHost()
            val socket = ServerSocket(port,50, address)

            while (true) {
                val client = socket.accept()
                val ibs = BufferedInputStream(client.getInputStream())
                val buffer = ByteArray(1024)
                val request = StringBuilder()

                while (ibs.read(buffer) != -1){
                    request.append(String(buffer))
                }

                if (request.isNotEmpty()) {
                    main.show()
                    println("Showing Main Form")
                }

//                thread {
//                    println("Showing Main Form")
////                    ui.taskManager = taskManager
//                    main.show()
//
//                }

            }
        }catch (e : Exception){
            println(e.message)
        }
    }

}