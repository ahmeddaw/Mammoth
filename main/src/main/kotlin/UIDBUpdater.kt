class UIDBUpdater(val task: DownloadTask, private val item: DownloadItem) : Thread() {
    var running = true
    var count = 0f
    var total = 0f
    init {
//        isDaemon = false

    }

    override fun run() {
        while (task.isAlive) {
            println("Updating UI ******************************************")
            item.apply {
                progress = 100f*count/total
                comment = total.toString()
                title = task.filename

            }
            sleep(100)
        }

    }





}