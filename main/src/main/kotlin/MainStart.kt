import javafx.collections.FXCollections
import javafx.collections.ListChangeListener
import javafx.event.ActionEvent
import javafx.event.EventType
import javafx.fxml.FXML
import javafx.fxml.Initializable
import javafx.scene.control.*
import javafx.scene.control.cell.PropertyValueFactory
import javafx.scene.image.ImageView
import java.awt.event.MouseEvent
import java.lang.Thread.sleep
import java.net.URL
import java.util.*
import kotlin.concurrent.thread


class MainStart : Initializable{
    @FXML lateinit var categoryList: ListView<String>
    @FXML lateinit var startButton : Button
    @FXML lateinit var startButtonImage : ImageView
    @FXML lateinit var itemsTable:TableView<DownloadItem>
    @FXML lateinit var tableIdColumn: TableColumn<DownloadItem, String>
    @FXML lateinit var tableNameColumn: TableColumn<DownloadItem, String>
    @FXML lateinit var tableStateColumn: TableColumn<DownloadItem, String>
    @FXML lateinit var tableProgressColumn: TableColumn<DownloadItem, String>
    @FXML lateinit var tableCommentColumn: TableColumn<DownloadItem, String>
    @FXML lateinit var youtubeVideoBtn: MenuItem
    @FXML lateinit var youtubeListBtn: MenuItem

    var allTasks = ArrayList<DownloadTask>()
    set(value) {
        field = value
        print("Tasks Count (SETTER): ")
        println(allTasks.size)
    }
    val categorylist = FXCollections.observableArrayList<String>()!!
    val itemsList = FXCollections.observableArrayList<DownloadItem>()!!
    lateinit var item : DownloadItem
    val taskManager = DownLoadTaskManager()
    override fun initialize(location: URL?, resources: ResourceBundle?) {


//        startButton.graphic = ImageView(img)

        val list = DBManager()
        categorylist.addAll(list.getCategorys())
        categoryList.items = categorylist

        tableIdColumn.cellValueFactory = PropertyValueFactory<DownloadItem, String>("id")
        tableNameColumn.cellValueFactory = PropertyValueFactory<DownloadItem, String>("title")
        tableStateColumn.cellValueFactory = PropertyValueFactory<DownloadItem, String>("state")
        tableProgressColumn.cellValueFactory = PropertyValueFactory<DownloadItem, String>("progress")
        tableCommentColumn.cellValueFactory = PropertyValueFactory<DownloadItem, String>("comment")







        val items =  list.getItems()
        itemsList.addAll(items)
        itemsTable.items  = itemsList





    }

    @FXML
    private fun addLinkEvent() {

//        print("Tasks Count : ")
//        println(allTasks.size)
        val dialog = EnterUrlDialog()
        val result = dialog.showAndWait()


        val task =DownloadTask(result.get().first, result.get().second)
        taskManager.addTask(task)

        val item = DBManager().createItem(task.filename,result.get().first)
//        uiUpdater = UIDBUpdater(item)

// https://www.youtube.com/watch?v=FLCmD5S_xzU
//        task.addOnProgressListener( object: DownloadTask.OnProgressListener{
//            override fun onProgressListener(count: Float, total: Float) {
//                print("Listner : ")
//                print(100f*count/total)
//                item!!.progress = 100f*count/total
//                DBManager().refresh(item)
//                itemsList[1].title = (100*count/total).toString()
//                println("%")
//            }
//        })
        var count = 0f
        var total = 0f

        task.addOnProgressListener { c, t ->

            if((c - count)/t > 0.0001) {
                count = c
                total = t
                item?.apply {
                    progress = 100f * count / total
                    comment = total.toString()
                    title = task.filename

                }
                itemsTable.refresh()

            }
            println("some Data ${item?.progress}, $total")

        }
//        if (!taskManager.isRunning)
//            taskManager.start()

//        thread(true){
////            print("Listener : ***********************************************")
//            while (task.isAlive){
//                item?.apply {
//                    progress = 100f*count/total
//                    comment = total.toString()
//                    title = task.filename
//
//                }
////                print("Listener : ")
////                print(100f*count/total)
////                println("%")
//                sleep(50)
//                itemsTable.refresh()
//            }
//        }
//        val t = object : Thread(){
//            override fun run() {
//                while (task.isAlive){
//                    item?.apply {
//                        progress = 100f*count/total
//                        comment = total.toString()
//                        title = task.filename
//
//                    }
//                    print("Listener : ")
//                    print(100f*count/total)
//                    println("%")
//                    sleep(100)
//                }
//            }
//        }
//        t.start()




//        task.addOnPostListener {
            //uiUpdateThread.start()
//            uiUpdater.start()
//        }

//        task.addOnAfterListener { count, total ->
////            item?.apply {
////                progress = 100f*count/total
////                comment = total.toString()
////                title = task.filename
////
////            }
//
//        }
//        uiUpdater.addItem(item!!, total, count){
//            it.forEach { t, u ->
//                println("result $t + $u")
//            }
//        }
//        task.start()
        itemsTable.items.add(item)





    }




    @FXML fun resaveItem(e : ActionEvent){
        object :Thread(){
            override fun run() {
                for (i in 0..150)
                    itemsList[2].title = (i).toString()
            }
        }.start()

    }













}