import javafx.event.ActionEvent
import javafx.event.EventHandler
import javafx.fxml.FXML
import javafx.fxml.FXMLLoader
import javafx.fxml.Initializable
import javafx.scene.Node
import javafx.scene.Parent
import javafx.scene.control.Button
import javafx.scene.control.ButtonType
import javafx.scene.control.Dialog
import javafx.scene.control.TextField
import javafx.stage.DirectoryChooser
import java.net.URL
import java.util.*


class EnterUrlDialog: Dialog<Pair<String, String>>() , Initializable{
    var urlField: TextField
    var directoryField: TextField
    private var chooseDirectory : Button


    override fun initialize(location: URL?, resources: ResourceBundle?) {


    }

    init {
        val root = FXMLLoader.load<Parent>(javaClass.getResource("views/enter_url_dialog.fxml"))

        val queue = ButtonType("Add to Queue")

        this.dialogPane.buttonTypes.add(queue)
        this.dialogPane.buttonTypes.add(ButtonType.OK)
        this.dialogPane.buttonTypes.add(ButtonType.CLOSE)
        val closeButton = this.dialogPane.lookupButton(ButtonType.CLOSE)
        closeButton.managedProperty().bind(closeButton.visibleProperty())

        urlField = root.lookup("#urlField") as TextField
        directoryField = root.lookup("#directoryField") as TextField
        directoryField.text = System.getProperty("user.home") + "/Downloads"

        chooseDirectory = root.lookup("#chooseDirectory") as Button
        chooseDirectory.addEventHandler(ActionEvent.ACTION){ _ ->
            val chooser = DirectoryChooser()
            val file = chooser.showDialog(chooseDirectory.scene.window)
            directoryField.text = file.absolutePath
        }

        val okButton = this.dialogPane.lookupButton(ButtonType.OK)
        val queueButton = this.dialogPane.lookupButton(queue)


        queueButton.addEventFilter(ActionEvent.ACTION){e ->
            if (!validateAndStore()) {
                e.consume()
                requireValidate()
            }

        }


        okButton.addEventFilter(ActionEvent.ACTION) { event ->
            if (!validateAndStore()) {
                event.consume()
                requireValidate()
            }
        }





        

        setResultConverter {

            return@setResultConverter  Pair(urlField.text, directoryField.text)
        }

        this.dialogPane.content = root
    }



    private fun validateAndStore(): Boolean {
        return urlField.text.length > 3  && !directoryField.text.isNullOrEmpty()
    }

    private fun requireValidate(){
        if(urlField.text.length < 3)
            urlField.requestFocus()
        else if (directoryField.text.isEmpty() || directoryField.text.isBlank())
            directoryField.requestFocus()
    }



}