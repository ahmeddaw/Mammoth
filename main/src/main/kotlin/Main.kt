import javafx.application.Application
import javafx.application.Platform
import javafx.fxml.FXMLLoader
import javafx.scene.Scene
import javafx.stage.Stage

class Main : Application() {


    init {
        println("An instance created ... ")
    }
    var show = false
    val taskManager: DownLoadTaskManager = DownLoadTaskManager()
//    set(value) {
//        println("Service to Main SETTER")
//        field = value
//        controller.allTasks = value!!.allTasks
//    }

    lateinit var mStage: Stage

    private var controller: MainStart = MainStart()
    override fun start(stage: Stage) {
        this.mStage = stage
        val loader = FXMLLoader(javaClass.getResource("views/main.fxml"))
//        val root = FXMLLoader.load<Parent>(javaClass.getResource("views/main.fxml"))
        loader.setController(controller)
        val scene = Scene(loader.load(),800.0,600.0)
        stage.scene = scene
        stage.title = "Mammoth"
//        scene.stylesheets.add(javaClass.getResource("style/main.css").toExternalForm())
        stage.show()
//        AquaFx.style()

        stage.setOnCloseRequest {
            it.consume()
            stage.hide()
            println("hide stage")

            Platform.runLater {
                while (!show){
                    Thread.sleep(100)
                }
                stage.show()
                stage.isAlwaysOnTop = true
                println("Show stage")
                show = false
                stage.isAlwaysOnTop = false

            }
        }

        val service = MammothService(this)
        service.start()




    }




//    fun launch() {
//
//        Application.launch()
//    }

    fun show() {
        show = true
    }


    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
//            val db = DBManager()
            launch(Main::class.java)

        }
    }
}